package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestIndexHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IndexHandler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Index handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected, err := ioutil.ReadFile("../client/index.html")

	if err != nil {
		panic(err)
	}

	if rr.Body.String() != string(expected) {
		t.Errorf("Index handler returned unexpected body: got %v want %v", rr.Body.String(), expected)
	}
}

func MakePostRequest(body []byte, path string, t *testing.T) {
	req, err := http.NewRequest("POST", path, bytes.NewBuffer(body))
	req.Header.Set("Content-Type", "application/json")

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(EventHandler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Event handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func GenerateBody(e Event, t *testing.T) []byte {
	b, err := json.Marshal(e)

	if err != nil {
		t.Fatal(err)
	}

	return b
}

func GetTestEvents() map[string]Event {
	var events map[string]Event = make(map[string]Event)
	//Test resize event
	events["resize"] = Event{
		WebsiteUrl: "https://ravelin.com",
		SessionId:  "123123-123123-123123123",
		EventType:  "resize",
		ResizeFrom: Dimension{
			Width:  "100",
			Height: "100"},
		ResizeTo: Dimension{
			Width:  "200",
			Height: "200"}}

	//Test copy and paste event
	events["copyAndPaste"] = Event{
		WebsiteUrl: "https://ravelin.com",
		SessionId:  "123123-123123-123123123",
		EventType:  "copyAndPaste",
		Pasted:     true,
		FormId:     "test-field"}

	//Test time taken event
	events["time"] = Event{
		WebsiteUrl: "https://ravelin.com",
		SessionId:  "123123-123123-123123123",
		EventType:  "timeTaken",
		Time:       72}

	return events
}

func TestProcessEvent(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	log.SetFlags(0)
	defer func() {
		log.SetOutput(os.Stderr)
	}()
	var expected string
	events := GetTestEvents()

	ProcessEvent(events["resize"])
	expected = "Request received {WebsiteUrl:https://ravelin.com SessionId:123123-123123-123123123 ResizeFrom:{Width:100 Height:100} ResizeTo:{Width:200 Height:200} CopyAndPaste:map[] FormCompletionTime:0}\n"

	if buf.String() != expected {
		t.Errorf("Event processor printed incorrect value: got %v want %v", buf.String(), expected)
	}
	buf.Reset()

	ProcessEvent(events["copyAndPaste"])
	expected = "Request received {WebsiteUrl:https://ravelin.com SessionId:123123-123123-123123123 ResizeFrom:{Width:100 Height:100} ResizeTo:{Width:200 Height:200} CopyAndPaste:map[test-field:true] FormCompletionTime:0}\n"

	if buf.String() != expected {
		t.Errorf("Event handler printed incorrect value: got %v want %v", buf.String(), expected)
	}
	buf.Reset()

	ProcessEvent(events["time"])
	expected = "Request received {WebsiteUrl:https://ravelin.com SessionId:123123-123123-123123123 ResizeFrom:{Width:100 Height:100} ResizeTo:{Width:200 Height:200} CopyAndPaste:map[test-field:true] FormCompletionTime:72}\n"
	if buf.String() != expected {
		t.Errorf("Event handler printed incorrect value: got %v want %v", buf.String(), expected)
	}
	buf.Reset()
}

func TestEventHandler(t *testing.T) {
	events := GetTestEvents()
	var body = GenerateBody(events["resize"], t)
	MakePostRequest(body, "/event", t)

	body = GenerateBody(events["copyAndPaste"], t)
	MakePostRequest(body, "/event", t)

	body = GenerateBody(events["time"], t)
	MakePostRequest(body, "/event", t)

}

func TestSessionRegistrationHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/session", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(IndexHandler)
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Session Registration handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	if rr.Body.String() == GenerateSessionId() {
		t.Error("Session Ids are being duplicated")
	}
}
