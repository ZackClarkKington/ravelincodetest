package main

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"sync"
)

type Dimension struct {
	Width  string `json:"width"`
	Height string `json:"height"`
}

type Data struct {
	WebsiteUrl         string          `json:"websiteUrl"`
	SessionId          string          `json:"sessionId"`
	ResizeFrom         Dimension       `json:"resizeFrom"`
	ResizeTo           Dimension       `json:"resizeTo"`
	CopyAndPaste       map[string]bool `json:"copyAndPaste"`
	FormCompletionTime int             `json:"time"`
}

type Event struct {
	EventType  string    `json: "eventType"`
	WebsiteUrl string    `json: "websiteUrl"`
	SessionId  string    `json: "sessionId"`
	Pasted     bool      `json: "pasted"`
	FormId     string    `json: "formId"`
	Time       int       `json: "time"`
	ResizeFrom Dimension `json:"resizeFrom"`
	ResizeTo   Dimension `json:"resizeTo"`
}

var sessions sync.Map

/*
	main() - Server entrypoint:
	- Initialises the session map
	- Initialises routes
	- Starts the server
*/
func main() {
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/event", EventHandler)
	http.HandleFunc("/styles.css", StylesHandler)
	http.HandleFunc("/app.js", ScriptHandler)
	http.HandleFunc("/session", SessionRegistrationHandler)
	fmt.Println("Server now running on localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func StylesHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "../client/styles.css")
}

func ScriptHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "../client/app.js")
}

/*
	Merge(a,b) - Merges two maps together:
	- Assumes that b is the newer map and so all values from b will override values from a
*/
func Merge(a map[string]bool, b map[string]bool) map[string]bool {
	var toReturn map[string]bool = make(map[string]bool)

	for k, v := range a {
		toReturn[k] = v
	}

	for k, v := range b {
		toReturn[k] = v
	}

	return toReturn
}

/*
	UpdateDimension(d1, d2) - Updates the fields in a dimension struct, if necessary
*/
func UpdateDimension(d1 Dimension, d2 Dimension) (d3 Dimension) {
	oldDimension := reflect.ValueOf(d1)
	newDimension := reflect.ValueOf(d2)
	toReturn := reflect.ValueOf(&d3).Elem()

	for i := 0; i < oldDimension.NumField(); i++ {
		if oldDimension.Field(i).String() != "" {
			toReturn.Field(i).Set(oldDimension.Field(i))
		} else {
			toReturn.Field(i).Set(newDimension.Field(i))
		}
	}

	return
}

/*
	UpdateSession - Updates the fields in a session struct, if necessary
*/
func UpdateSession(d1 Data, d2 Data) (d3 Data) {
	oldSession := reflect.ValueOf(d1)
	newSession := reflect.ValueOf(d2)
	toReturn := reflect.ValueOf(&d3).Elem()

	for i := 0; i < oldSession.NumField(); i++ {
		switch oldSession.Field(i).Type() {
		case reflect.TypeOf(""):
			str := newSession.Field(i).Interface().(string)
			if str != "" {
				toReturn.Field(i).SetString(str)
			} else {
				toReturn.Field(i).Set(oldSession.Field(i))
			}
			break
		case reflect.TypeOf(0):
			num := newSession.Field(i).Int()
			if num != 0 {
				toReturn.Field(i).SetInt(num)
			} else {
				toReturn.Field(i).Set(oldSession.Field(i))
			}
			break
		default:
			if oldSession.Field(i).Type().String() == "map[string]bool" {
				oldMap := oldSession.Field(i).Interface().(map[string]bool)
				newMap := newSession.Field(i).Interface().(map[string]bool)
				toReturn.Field(i).Set(reflect.ValueOf(Merge(oldMap, newMap)))
			} else if oldSession.Field(i).Type().String() == "main.Dimension" {
				oldDimension := oldSession.Field(i).Interface().(Dimension)
				newDimension := newSession.Field(i).Interface().(Dimension)
				toReturn.Field(i).Set(reflect.ValueOf(UpdateDimension(oldDimension, newDimension)))
			}
		}
	}

	return
}

//IndexHandler() - Serves the static file ../client/index.html
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "../client/index.html")
}

/*
	EventHandler:
	- Receives POST requests made to the /event path
	- Decodes JSON body and populates event struct
	- Spawns new goroutine to process event
*/
func EventHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unable to read body"))
		return
	}

	req := &Event{}
	if err = json.Unmarshal(body, req); err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Unable to unmarshal JSON request"))
		return
	}
	go ProcessEvent(*req)
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Success"))
}

/*
	ProcessEvent:
	- Receives event struct and uses it to update existing session/create new session
	- Logs session to stdout
*/
func ProcessEvent(e Event) {
	newSession := &Data{}
	newSession.CopyAndPaste = make(map[string]bool)
	newSession.SessionId = e.SessionId
	newSession.WebsiteUrl = e.WebsiteUrl

	if e.EventType == "copyAndPaste" {
		newSession.CopyAndPaste[e.FormId] = e.Pasted
	} else if e.EventType == "timeTaken" {
		newSession.FormCompletionTime = e.Time
	} else if e.EventType == "resize" {
		newSession.ResizeFrom = e.ResizeFrom
		newSession.ResizeTo = e.ResizeTo
	}

	if oldSession, ok := sessions.Load(newSession.SessionId); ok {
		sessions.Store(newSession.SessionId, UpdateSession(oldSession.(Data), *newSession))
	} else {
		sessions.Store(newSession.SessionId, *newSession)
	}

	updatedSession, ok := sessions.Load(newSession.SessionId)

	if ok {
		log.Printf("Request received %+v", updatedSession.(Data))
	} else {
		log.Println("Request received, error occurred whilst fetching updated session from store")
	}
}

//Generates a session id which will be used to identify each form session
func GenerateSessionId() string {
	b := make([]byte, 16)
	_, err := rand.Read(b)

	if err != nil {
		log.Println("Failed to read random values for session id")
		return "err"
	}

	sessionId := fmt.Sprintf("%x-%x-%x-%x-%x", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

	//No session id should be used more than once, recursively call until id is unique
	if _, ok := sessions.Load(sessionId); ok {
		sessionId = GenerateSessionId()
	}

	return sessionId
}

//Generates a session id and sends it to the client
func SessionRegistrationHandler(w http.ResponseWriter, r *http.Request) {
	sessId := GenerateSessionId()
	if sessId == "err" {
		w.WriteHeader(http.StatusBadGateway)
		w.Write([]byte("Unable to generate session id"))
	} else {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, sessId)
	}
	return
}
