var sessId;
var startTime = 0;

function guid(){
	$.get("/session", function(data){
		sessId = data;
	});
}

function setStartTime(e){
	if(startTime == 0){
		startTime = new Date();
	}
}

var originalDimensions = {
	width: window.innerWidth.toString(),
	height: window.innerHeight.toString()
};

var url = "https://ravelin.com";

function submitForm(e){
	e.preventDefault();
	transmitEvent({"type": "submit"});
}

function initEventListeners(){
	window.addEventListener("resize", transmitEvent);
	window.addEventListener("paste", transmitEvent);
	window.addEventListener("copy", transmitEvent);
	window.addEventListener("keypress", setStartTime);
	window.addEventListener("submit", submitForm);
}

function transmitEvent(e){
	var event = {
		"websiteUrl": url,
		"sessionId": sessId
	};
	console.log(e);

	switch(e.type){
		case "resize":
			event.eventType = "resize";
			event.resizeFrom = originalDimensions;
			event.resizeTo = {
				width: window.innerWidth.toString(),
				height: window.innerHeight.toString()
			};
			break;
		case "paste":
			event.eventType = "copyAndPaste";
			event.pasted = true;
			event.formId = e.target.id
			break;
		case "copy":
			event.eventType = "copyAndPaste";
			event.pasted = false;
			event.formId = e.target.id;
			break;
		case "submit":
			event.eventType = "timeTaken";
			var endTime = new Date();
			event.time = Math.round((endTime.getTime() - startTime.getTime())/ 1000);
			break;
	}
	$.ajax({
		type: "POST",
		url: "/event",
		data: JSON.stringify(event),
		dataType: "text",
		contentType: "application/json"
	});
}

guid();
initEventListeners();